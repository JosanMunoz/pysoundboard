import os
import sys
import ctypes
from ctypes import wintypes
import win32con
import pyaudio
import wave
import _thread

byref = ctypes.byref
user32 = ctypes.windll.user32

HOTKEYS = {
  1 : (win32con.VK_F4, win32con.MOD_WIN),
  2 : (win32con.VK_NUMPAD9, win32con.NULL),
  3 : (win32con.VK_NUMPAD6, win32con.NULL),
  4 : (win32con.VK_NUMPAD8, win32con.NULL)
}

def PlayWav(filename):
  CHUNK = 1024

  if filename is None:
      #print("Plays a wave file.\n\nUsage: %s filename.wav" % filename)
      sys.exit()
      return

  wf = wave.open(filename, 'rb')

  # instantiate PyAudio (1)
  p = pyaudio.PyAudio()

  # open stream (2)
  stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                  channels=wf.getnchannels(),
                  rate=wf.getframerate(),
                  output=True)

  # read data
  data = wf.readframes(CHUNK)

  # play stream (3)
  while len(data) > 0:
      stream.write(data)
      data = wf.readframes(CHUNK)

  # stop stream (4)
  stream.stop_stream()
  stream.close()

  # close PyAudio (5)
  p.terminate()
  sys.exit()
#End of PlayWav(filename)

HOTKEY_ACTIONS = {
  1 : handle_win_6,
  2 : handle_win_f4,
  3 : handle_win_9
}

HOTKEY_FILENAMES = {
  2 : "rimshot.wav",
  3 : "item_catch.wav",
  4 : "Yuudachi-Poi.wav"
}

#
# RegisterHotKey takes:
#  Window handle for WM_HOTKEY messages (None = this thread)
#  arbitrary id unique within the thread
#  modifiers (MOD_SHIFT, MOD_ALT, MOD_CONTROL, MOD_WIN)
#  VK code (either ord ('x') or one of win32con.VK_*)
#
for id, (vk, modifiers) in HOTKEYS.items ():
  print ("Registering id", id, "for key", vk)
  if not user32.RegisterHotKey (None, id, modifiers, vk):
    print ("Unable to register id", id)

#
# Home-grown Windows message loop: does
#  just enough to handle the WM_HOTKEY
#  messages and pass everything else along.
#
try:
  msg = wintypes.MSG ()
  while user32.GetMessageA (byref (msg), None, 0, 0) != 0:
    if msg.message == win32con.WM_HOTKEY:
      #action_to_take = HOTKEY_ACTIONS.get (msg.wParam)
      File_to_Play = (HOTKEY_FILENAMES.get (msg.wParam),)
      #print("msg param : " + str(msg.wParam) + " type : " + str(type(msg.wParam)))
      if msg.wParam == 1:
        user32.PostQuitMessage (0)
        sys.exit
        
      if File_to_Play:#action_to_take:
        #action_to_take ()
        #print("fileto " + str(File_to_Play))
        #tup = (str(File_to_Play));
        _thread.start_new_thread( PlayWav, File_to_Play )

    user32.TranslateMessage (byref (msg))
    user32.DispatchMessageA (byref (msg))

finally:
  for id in HOTKEYS.keys ():
    user32.UnregisterHotKey (None, id)
