Purpose:
SoundBite, the PySoundBoard in general, is intended to be a hotkeyed soundboard for windows platforms. The Soundboard supports WAV files in a multithreaded fashion.

Dependencies:
	- 	Python
	-	PyAudio
	-	PyWin32

Future Improvements:
	- PyGUI implementation to ease the creation and use of the Soundboard
	- Saving/Loading Sound bites and hotkeys
	- Looping Soundbites
	- MP3 support